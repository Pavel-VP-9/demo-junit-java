import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testSum1() {
       Calculator cal = new Calculator();
       double result = cal.sum(3,5.5);
        Assert.assertEquals(8.5,result, 0.01);

    }
    @Test
    public void TestSubtraction() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtraction(6, 3);
        Assert.assertEquals(3, actual);

    }
    @Test
    public void TestMultiplication() {
        Calculator calculator = new Calculator();
        long actual = calculator.multiplication(6, 3);
        Assert.assertEquals(18, actual);
    }
    @Test
    public void TestDivision() {
        Calculator calculator = new Calculator();
        int actual = calculator.division(6, 3);
        Assert.assertEquals(2, actual);
    }
    @Test
    public void testSum2(){
        Calculator calculator=new Calculator(11,6);
        int actual = calculator.sum();
        Assert.assertEquals(17, actual);
    }
    @Test
    public void testSubtraction2() {
        Calculator calculator = new Calculator(10, 6);
        int actual = calculator.subtraction();
        Assert.assertEquals(4, actual);
    }
    @Test
    public void testMultiplication2(){
        Calculator calculator = new Calculator(3,3);
            int actual = calculator.multiplication();
        Assert.assertEquals(9, actual);
        }
        @Test
    public  void testDivision2(){
        Calculator calculator = new Calculator(9,3);
        int actual =calculator.division();
        Assert.assertEquals(3, actual);

    }

}
